package es.cloudtf.pcfutbol.pcftuboleureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class PcftubolEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PcftubolEurekaApplication.class, args);
	}

}
